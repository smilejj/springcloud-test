package me.smileface;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.concurrent.Future;

@Slf4j
@Component
public class TestFilter implements GlobalFilter, Ordered {

    @Autowired
    private ClientHolder clientHolder;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        callClient();
        return chain.filter(exchange);
    }

    private void callClient() {
        Future<String> stringFuture = clientHolder.systemClientTest();
        try {
            log.info("stringFuture.rs :{}", stringFuture.get());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}
