package me.smileface;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "system", decode404 = true)
public interface SystemFeignClient {

    @GetMapping("/test")
    String test(@RequestParam("id") String id);

}
