package me.smileface;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import java.util.concurrent.Future;

@Slf4j
@Component
public class ClientHolder {

    @Lazy
    @Autowired
    private SystemFeignClient systemFeignClient;

    @Async
    public Future<String> systemClientTest() {
        log.info("开始使用SystemClient...");
        String s = systemFeignClient.test("1001");
        return new AsyncResult<>(s);
    }
}
