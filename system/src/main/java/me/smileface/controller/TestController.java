package me.smileface.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class TestController {

    @GetMapping("/test")
    public String test(@RequestParam("id") String id) {
        log.info("收到ID = {}", id);
        return "test: " + id;
    }

    @GetMapping("/test2")
    public String test2() {
        return "test2";
    }

}
